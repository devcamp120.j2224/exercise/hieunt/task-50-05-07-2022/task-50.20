package Package_Task50_20;

public class NewDevcampApp {
    public static void main(String[] args) {
        System.out.println("Làm task 50.20");
        showName("Hieu");
        NewDevcampApp person = new NewDevcampApp();
        person.showName("Hieu", 30);
    }
    public static void showName(String name) {
        System.out.println("My name is " + name);
    }
    public void showName(String name, int age) {
        System.out.println("My name is " + name + ". I'm " + age + " years old");
    }
}
