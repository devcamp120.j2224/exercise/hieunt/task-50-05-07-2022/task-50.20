import Package_Task50_20.NewDevcampApp;

public class App {
    public static void main(String[] args) throws Exception {
        // đây là biến string
        String name = "DevCamp";
        System.out.println("Name là " + name); // ghi ra biến name
        System.out.println("Name có length là " + name.length()); // ghi ra độ dài string name
        System.out.println("Name có Upper Case là " + name.toUpperCase()); // ghi ra chữ in to
        System.out.println("Name có Lower Case là " + name.toLowerCase()); // ghi ra chữ in nhỏ
        NewDevcampApp.showName(name);
        NewDevcampApp newPerson = new NewDevcampApp();
        newPerson.showName(name, 39);
    }
}
